package com.zatek.wikicrawl.base

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.zatek.wikicrawl.App
import com.zatek.wikicrawl.components.AppComponent
import com.zatek.wikicrawl.components.DaggerAppComponent
import com.zatek.wikicrawl.components.ViewModelInjector
import com.zatek.wikicrawl.modules.ApiModule
import com.zatek.wikicrawl.modules.AppModule
import com.zatek.wikicrawl.modules.GlideModule

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
    }

    abstract fun bind()
    abstract fun setObservers()
    abstract fun setListeners()
}