package com.zatek.wikicrawl.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.zatek.wikicrawl.model.ArticleImage
import com.zatek.wikicrawl.model.ArticleResponse

@Dao
interface ArticleDao {
    @get:Query("SELECT * FROM articleresponse")
    val articles: LiveData<List<ArticleResponse>>

    @Insert
    fun insertArticles(vararg articles: ArticleResponse)

    @Delete
    fun deleteArticles(vararg articles: ArticleResponse)

    @Query("SELECT * FROM articleresponse WHERE id = :articleId")
    fun articleById(articleId: Int): ArticleResponse

    @Query("SELECT * FROM articleimage WHERE articleId = :articleId")
    fun imagesInArticle(articleId: Int): List<ArticleImage>

    @Insert
    fun insertImages(vararg images: ArticleImage)

    @Delete
    fun deleteImages(vararg articles: ArticleImage)
}