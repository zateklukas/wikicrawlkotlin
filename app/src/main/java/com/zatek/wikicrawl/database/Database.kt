package com.zatek.wikicrawl.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.zatek.wikicrawl.model.ArticleImage
import com.zatek.wikicrawl.model.ArticleResponse

@Database(entities = [ArticleResponse::class, ArticleImage::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun articlesDao(): ArticleDao
}