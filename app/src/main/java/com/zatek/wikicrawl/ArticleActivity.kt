package com.zatek.wikicrawl

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.zatek.wikicrawl.base.BaseActivity
import com.zatek.wikicrawl.base.BaseViewModelFactory
import com.zatek.wikicrawl.databinding.ActivityArticleBinding
import com.zatek.wikicrawl.adapters.ArticleImageAdapter
import com.zatek.wikicrawl.view_models.ArticleViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ArticleActivity : BaseActivity() {

    private lateinit var binding: ActivityArticleBinding
    private lateinit var viewModel: ArticleViewModel
    private lateinit var adapter: ArticleImageAdapter
    private var articleId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_article)

        articleId = intent.getIntExtra("article_id",0)

        val context = this
        viewModel = ViewModelProviders.of(this, BaseViewModelFactory{ ArticleViewModel(context) }).get(ArticleViewModel::class.java)

        adapter = ArticleImageAdapter(this)

        bind()
        setObservers()
        setListeners()
    }


    override fun bind() {
        viewModel.bind(articleId)
        binding.viewModel = viewModel
        binding.pager.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        binding.pager.adapter = adapter
    }

    override fun setObservers() {
        viewModel.isLiked.observe(this, Observer {
            viewModel.toggleLike(it)
        })
        viewModel.articleImages.observe(this, Observer {
            adapter.updateImages(it)
            adapter.notifyDataSetChanged()
        })
    }

    override fun setListeners() {
        binding.arrowBack.setOnClickListener {finish()}
        binding.likeButton.setOnClickListener {
            GlobalScope.launch {
                viewModel.save()
            }
        }
    }

}
