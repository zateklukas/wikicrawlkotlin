package com.zatek.wikicrawl.modules

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
object GlideModule {
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideGlideInterface(context: Context): RequestManager {
        return Glide.with(context).setDefaultRequestOptions(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
    }

}
