
package com.zatek.wikicrawl.modules

import android.content.Context
import androidx.room.Room
import com.zatek.wikicrawl.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
object DatabaseModule {
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRoomInterface(context: Context): AppDatabase {
        return Room.databaseBuilder(context,AppDatabase::class.java, "articlesDb").build()
    }

}
