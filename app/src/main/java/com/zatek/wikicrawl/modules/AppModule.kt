package com.zatek.wikicrawl.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(application: Application) {
    var context : Context = application.applicationContext

    @Singleton
    @Provides
    fun provideContext() : Context {
        return context
    }
}
