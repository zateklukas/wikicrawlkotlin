package com.zatek.wikicrawl.utils

import android.graphics.Bitmap
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestBuilder
import com.zatek.wikicrawl.utils.extensions.getParentActivity

@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>){
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    parentActivity?.let {
        visibility.observe(it, Observer { value -> view.visibility = value})
    }
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity:AppCompatActivity? = view.getParentActivity()
    parentActivity?.let {
        text?.observe(it, Observer { value -> view.text = value ?: "" })
    }
}

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}

@BindingAdapter("mutableImage")
fun setMutableImage(view: AppCompatImageView, request: MutableLiveData<RequestBuilder<Bitmap>?>) {
    val parentActivity:AppCompatActivity? = view.getParentActivity()
    parentActivity?.let {
        request.observe(it, Observer { value -> value?.into(view) })
    }
}

@BindingAdapter("mutableImage")
fun setMutableImage(view: ImageButton, request: MutableLiveData<Int>?) {
    val parentActivity:AppCompatActivity? = view.getParentActivity()
    parentActivity?.let {
        request?.observe(it, Observer { value -> view.setImageResource(value) })
    }
}