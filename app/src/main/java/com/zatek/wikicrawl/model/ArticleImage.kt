package com.zatek.wikicrawl.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ArticleImage(
    var articleId: Int,
    @PrimaryKey var url: String
)