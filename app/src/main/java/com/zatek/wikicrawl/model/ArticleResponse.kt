package com.zatek.wikicrawl.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import org.json.JSONObject

@Entity(tableName = "articleresponse")
data class ArticleResponse(
    @PrimaryKey
    var id: Int,
    var title: String,
    var content: String,
    var thumbnail: String,
    var language: String,
    @Ignore
    var images: List<String> = listOf()
) {
    constructor() : this(0, "", "", "", "", listOf())

    companion object {
        fun fromJson(json: JSONObject): ArticleResponse {
            val id = json.getInt("pageid")
            val title = json.getString("title")
            val content = json.getString("extract")
            var thumbnail = ""
            if (json.has("thumbnail")) {
                val thumbnailObj = json.getJSONObject("thumbnail")
                thumbnail = thumbnailObj.getString("source")
            }
            val images = ArrayList<String>()
            if (json.has("images")) {
                val imageObjects = json.getJSONArray("images")
                for (i in 0 until imageObjects.length()) {
                    images.add(imageObjects.getJSONObject(i).getString("title"))
                }
            }
            return ArticleResponse(id, title, content, thumbnail, "en", images) //TODO replace
        }
    }
}