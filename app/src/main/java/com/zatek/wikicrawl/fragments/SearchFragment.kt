package com.zatek.wikicrawl.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zatek.wikicrawl.base.BaseViewModelFactory
import com.zatek.wikicrawl.databinding.FragmentSearchBinding
import com.zatek.wikicrawl.view_models.ArticleListViewModel


class SearchFragment : Fragment() {

    private lateinit var binding: FragmentSearchBinding
    private lateinit var viewModel: ArticleListViewModel

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, com.zatek.wikicrawl.R.layout.fragment_search, parent, false)
        binding.articleList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        binding.searchClick.setOnTouchListener { _, event -> handleSearchTouch(event) }
        binding.searchClick.setOnEditorActionListener { _, _, _ -> handleSearchSubmit() }

        binding.articleList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(50)) {
                    if (viewModel.loadingVisibility.value == View.GONE) {
                        viewModel.loadArticles(viewModel.actualQuery, viewModel.actualOffset + 10, true)
                    }
                }
            }
        })

        viewModel = ViewModelProviders.of(this, BaseViewModelFactory { ArticleListViewModel(context!!) })
            .get(ArticleListViewModel::class.java)

        binding.viewModel = viewModel
        return binding.root
    }

    private fun handleSearchSubmit(): Boolean {
        searchArticles(binding.searchClick.text.toString(), 0)
        return false
    }

    private fun handleSearchTouch(event: MotionEvent): Boolean {
        if (event.rawX >= (binding.searchClick.right - binding.searchClick.compoundDrawables[2].bounds.width())) {
            searchArticles(binding.searchClick.text.toString(), 0)
        }
        return false
    }

    private fun searchArticles(query: String, offset: Int) {
        viewModel.loadArticles(query, offset)
    }
}