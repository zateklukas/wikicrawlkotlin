package com.zatek.wikicrawl.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zatek.wikicrawl.base.BaseViewModelFactory
import com.zatek.wikicrawl.databinding.FragmentSavedBinding
import com.zatek.wikicrawl.view_models.SavedArticlesListViewModel

class SavedFragment : Fragment() {
    private lateinit var binding: FragmentSavedBinding
    private lateinit var viewModel: SavedArticlesListViewModel
    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, com.zatek.wikicrawl.R.layout.fragment_saved, parent, false)
        binding.articleList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        viewModel = ViewModelProviders.of(this, BaseViewModelFactory { SavedArticlesListViewModel(context!!) }).get(
            SavedArticlesListViewModel::class.java
        )

        binding.viewModel = viewModel
        return binding.root
    }
}