package com.zatek.wikicrawl.converters

import com.zatek.wikicrawl.model.ArticleImage
import com.zatek.wikicrawl.model.ArticleResponse
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.IllegalArgumentException
import java.lang.reflect.Type

class WikiArticleListConverterFactory : Converter.Factory() {

    override fun responseBodyConverter(
        type: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {

        if (type.toString() == "java.util.ArrayList<com.zatek.wikicrawl.model.ArticleResponse>")
            return WikiArticleListConverter.INSTANCE
        else if (type.toString() == "class com.zatek.wikicrawl.model.ArticleImage")
            return WikiArticleImageConverter.INSTANCE
        throw IllegalArgumentException("Converter of type $type does on exist")
    }

    class WikiArticleListConverter : Converter<ResponseBody, ArrayList<ArticleResponse>> {
        override fun convert(value: ResponseBody): ArrayList<ArticleResponse> {
            val root = JSONObject(value.string())
            val pages = root.getJSONObject("query").getJSONObject("pages")
            val pageIds = pages.keys()
            val articles: ArrayList<ArticleResponse> = ArrayList()
            pageIds.forEach {
                val articleJson = pages.getJSONObject(it)
                articles.add(ArticleResponse.fromJson(articleJson))
            }
            return articles
        }

        companion object {
            val INSTANCE: WikiArticleListConverter = WikiArticleListConverter()
        }
    }

    class WikiArticleImageConverter : Converter<ResponseBody, ArticleImage> {
        override fun convert(value: ResponseBody): ArticleImage {
            val root = JSONObject(value.string())
            val pages = root.getJSONObject("query").getJSONObject("pages")
            val pageIds = pages.keys()
            val articles: ArrayList<ArticleImage> = ArrayList()
            pageIds.forEach {
                val articleJson = pages.getJSONObject(it)
                val imageArray = articleJson.getJSONArray("imageinfo")
                if (imageArray.length() > 0) {
                    for (i in 0 until imageArray.length()) {
                        articles.add(ArticleImage(it.toInt(), imageArray.getJSONObject(i).getString("url")))
                    }
                }
            }
            return articles.first()
        }

        companion object {
            val INSTANCE: WikiArticleImageConverter = WikiArticleImageConverter()
        }
    }

}
