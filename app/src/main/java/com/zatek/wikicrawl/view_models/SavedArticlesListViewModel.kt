package com.zatek.wikicrawl.view_models

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.zatek.wikicrawl.App
import com.zatek.wikicrawl.adapters.ArticleListAdapter
import com.zatek.wikicrawl.base.BaseViewModel
import com.zatek.wikicrawl.components.DaggerViewModelInjector
import com.zatek.wikicrawl.database.AppDatabase
import com.zatek.wikicrawl.model.ArticleResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SavedArticlesListViewModel(context: Context) : BaseViewModel(context) {
    @Inject
    lateinit var db: AppDatabase

    private lateinit var subscription: Observer<List<ArticleResponse>>
    private lateinit var array: LiveData<List<ArticleResponse>>

    val articletListAdapter: ArticleListAdapter

    init {
        DaggerViewModelInjector.builder().appComponent((context.applicationContext as App).injector).build()
            .inject(this)
        loadArticles()
        articletListAdapter = ArticleListAdapter(context)
    }

    @SuppressLint("CheckResult")
    fun loadArticles() {
        Observable.fromCallable { db.articlesDao().articles }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { }
            .doOnTerminate { }
            .subscribe({ result ->
                array = result
                subscription = Observer {
                    articletListAdapter.updateList(ArrayList(it))
                }
                result.observeForever(
                    subscription
                )

            }, { })
    }

    override fun onCleared() {
        super.onCleared()
        array.removeObserver(subscription)
    }
}