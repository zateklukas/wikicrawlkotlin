package com.zatek.wikicrawl.view_models

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.zatek.wikicrawl.base.BaseViewModel

class MainViewModel(context: Context) : BaseViewModel(context) {
    var selectedPage = MutableLiveData<Int>()

    init {
        selectedPage.value = 0
    }
}