package com.zatek.wikicrawl.view_models

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.RequestManager
import com.zatek.wikicrawl.App
import com.zatek.wikicrawl.R
import com.zatek.wikicrawl.api.ArticleApi
import com.zatek.wikicrawl.base.BaseViewModel
import com.zatek.wikicrawl.components.DaggerViewModelInjector
import com.zatek.wikicrawl.database.AppDatabase
import com.zatek.wikicrawl.model.ArticleImage
import com.zatek.wikicrawl.model.ArticleResponse
import com.zatek.wikicrawl.utils.extensions.toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import javax.inject.Inject

class ArticleViewModel(context: Context) : BaseViewModel(context) {

    @Inject
    lateinit var glide: RequestManager
    @Inject
    lateinit var context: Context
    @Inject
    lateinit var articleApi: ArticleApi
    @Inject
    lateinit var db: AppDatabase

    val articleTitle = MutableLiveData<String>()
    val articleBody = MutableLiveData<String>()
    val articleImages = MutableLiveData<ArrayList<ArticleImage>>()
    var likeDrawable = MutableLiveData<Int>()
    var isLiked = MutableLiveData<Boolean>()

    private val uiScope = CoroutineScope(Dispatchers.Main)

    private lateinit var articleResponse: ArticleResponse

    private var disliked: Int = R.drawable.ic_favorite_border_black
    private var liked: Int = R.drawable.ic_favorite_red

    init {
        DaggerViewModelInjector.builder().appComponent((context.applicationContext as App).injector).build()
            .inject(this)
        isLiked.value = false
        likeDrawable.value = disliked
    }

    @SuppressLint("CheckResult")
    fun bind(articleId: Int) {
        Observable.fromCallable { db.articlesDao().articleById(articleId) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    articleResponse = response
                    isLiked.value = true
                    articleTitle.value = response.title
                    articleBody.value = response.content
                    Observable.fromCallable { db.articlesDao().imagesInArticle(articleId) }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            articleImages.value = it as ArrayList<ArticleImage>?
                        },
                            {

                            })
                },
                {
                    isLiked.value = false
                    populateFromServer(articleId)
                }
            )

    }

    @SuppressLint("CheckResult")
    fun populateFromServer(articleId: Int) {
        articleApi.getArticleDetail(articleId).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                run {
                    val articleResult = result.first()
                    articleResponse = articleResult
                    articleTitle.value = articleResult.title
                    articleBody.value = articleResult.content
                    val images = ArrayList<ArticleImage>()
                    articleResult.images.forEach {
                        articleApi.getImage(it).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ image ->
                                image.articleId = articleId
                                if (image.url.isNotEmpty() && !image.url.contains(".svg")) {
                                    images.add(image)
                                    articleImages.value = images
                                }
                            }, {
                                context.toast("Unknown error occured")
                            })
                    }
                }
            }
            ) { error ->
                print(error)
            }
    }

    fun toggleLike(isLiked: Boolean) {
        if (isLiked)
            likeDrawable.value = liked
        else
            likeDrawable.value = disliked
    }

    fun save() {
        uiScope.launch {
            async { saveToDb() }.await()
            isLiked.value = !isLiked.value!!
        }
    }

    private suspend fun saveToDb() = withContext(context = Dispatchers.IO) {
        if (!isLiked.value!!) {
            db.articlesDao().insertArticles(articleResponse)
            articleImages.value?.forEach {
                db.articlesDao().insertImages(it)
            }
        } else {
            db.articlesDao().deleteArticles(articleResponse)
            articleImages.value?.forEach {
                db.articlesDao().deleteImages(it)
            }
        }
    }

}

