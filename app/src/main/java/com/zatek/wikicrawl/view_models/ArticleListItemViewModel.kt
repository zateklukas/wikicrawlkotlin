package com.zatek.wikicrawl.view_models

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.RequestManager
import com.zatek.wikicrawl.App
import com.zatek.wikicrawl.R
import com.zatek.wikicrawl.base.BaseViewModel
import com.zatek.wikicrawl.components.DaggerViewModelInjector
import com.zatek.wikicrawl.model.ArticleResponse
import javax.inject.Inject

class ArticleListItemViewModel(context: Context) : BaseViewModel(context) {

    @Inject
    lateinit var glide: RequestManager
    @Inject
    lateinit var context: Context

    val articleTitle = MutableLiveData<String>()
    val articleBody = MutableLiveData<String>()
    val articleImage = MutableLiveData<RequestBuilder<Bitmap>>()

    init {
        DaggerViewModelInjector.builder().appComponent((context.applicationContext as App).injector).build()
            .inject(this)

    }

    fun bind(article: ArticleResponse) {
        if (article.thumbnail.isNotEmpty())
            articleImage.value = glide.asBitmap()
                .error(glide.asBitmap().load(context.getDrawable(R.drawable.ic_broken_image_black)).load(article.thumbnail))
                .load(Uri.parse(article.thumbnail))
        else
            articleImage.value = glide.asBitmap().load(context.getDrawable(R.drawable.ic_broken_image_black))
        articleTitle.value = article.title
        articleBody.value = article.content
    }
}