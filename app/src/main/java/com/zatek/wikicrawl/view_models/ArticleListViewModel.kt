package com.zatek.wikicrawl.view_models

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.zatek.wikicrawl.App
import com.zatek.wikicrawl.api.ArticleApi
import com.zatek.wikicrawl.base.BaseViewModel
import com.zatek.wikicrawl.components.DaggerViewModelInjector
import com.zatek.wikicrawl.adapters.ArticleListAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ArticleListViewModel(context: Context) : BaseViewModel(context) {
    @Inject
    lateinit var articleApi: ArticleApi

    private lateinit var subscription: Disposable

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val articletListAdapter: ArticleListAdapter

    var actualOffset = 0
    var actualQuery = ""

    init {
        DaggerViewModelInjector.builder().appComponent((context.applicationContext as App).injector).build().inject(this)
        loadArticles("Venice", 0)
        articletListAdapter =  ArticleListAdapter(context)
    }

    fun loadArticles(query: String, offset: Int, append: Boolean = false){
        if(loadingVisibility.value == 1) return
        subscription = articleApi
            .getArticles(query,offset)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{ loadingVisibility.value = View.VISIBLE }
            .doOnTerminate{ loadingVisibility.value = View.GONE}
            .subscribe({ result ->
                run {
                    if (!append)
                        articletListAdapter.updateList(result)
                    else
                        articletListAdapter.appendList(result, offset)
                    actualOffset = offset
                    actualQuery = query
                    loadingVisibility.value = View.GONE
                }
            }, { loadingVisibility.value = View.GONE })
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}