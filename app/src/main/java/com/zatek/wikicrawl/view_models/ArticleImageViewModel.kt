package com.zatek.wikicrawl.view_models

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.zatek.wikicrawl.App
import com.zatek.wikicrawl.R
import com.zatek.wikicrawl.base.BaseViewModel
import com.zatek.wikicrawl.components.DaggerViewModelInjector
import javax.inject.Inject

class ArticleImageViewModel(context: Context) : BaseViewModel(context) {

    @Inject
    lateinit var glide: RequestManager
    @Inject
    lateinit var context: Context

    val articleImage = MutableLiveData<RequestBuilder<Bitmap>>()

    init {
        DaggerViewModelInjector.builder().appComponent((context.applicationContext as App).injector).build()
            .inject(this)
    }

    fun bind(imageTitle: String) {
        articleImage.value = glide
            .asBitmap()
            .load(Uri.parse(imageTitle))
            .apply(
                RequestOptions()
                    .centerInside()
                    .override(600)
            )
            .error(
                glide
                    .asBitmap()
                    .apply(
                        RequestOptions()
                            .override(26)
                    )
                    .load(R.drawable.ic_broken_image_black)
            )
    }

}