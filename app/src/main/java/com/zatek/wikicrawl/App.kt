package com.zatek.wikicrawl

import android.app.Application
import com.zatek.wikicrawl.components.AppComponent
import com.zatek.wikicrawl.components.DaggerAppComponent
import com.zatek.wikicrawl.modules.ApiModule
import com.zatek.wikicrawl.modules.AppModule
import com.zatek.wikicrawl.modules.DatabaseModule
import com.zatek.wikicrawl.modules.GlideModule


class App : Application() {
    lateinit var injector : AppComponent
    override fun onCreate() {
        super.onCreate()
        injector = DaggerAppComponent.builder().appModule(AppModule(this)).databaseModule(DatabaseModule).articleModule(ApiModule).imageModule(GlideModule).build()
    }
}