package com.zatek.wikicrawl.components

import android.content.Context
import com.bumptech.glide.RequestManager
import com.zatek.wikicrawl.api.ArticleApi
import com.zatek.wikicrawl.database.AppDatabase
import com.zatek.wikicrawl.modules.ApiModule
import com.zatek.wikicrawl.modules.AppModule
import com.zatek.wikicrawl.modules.DatabaseModule
import com.zatek.wikicrawl.modules.GlideModule
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton


@Singleton
@Component(modules = [(AppModule::class),(GlideModule::class),(ApiModule::class), (DatabaseModule::class)])
interface AppComponent{
    fun articleApi(): ArticleApi
    fun retrofit(): Retrofit
    fun glide(): RequestManager
    fun context(): Context
    fun db(): AppDatabase
    @Component.Builder
    interface Builder{
        fun build(): AppComponent
        fun appModule(appModule: AppModule): Builder
        fun articleModule(apiModule: ApiModule): Builder
        fun imageModule(imageModule: GlideModule): Builder
        fun databaseModule(databaseModule: DatabaseModule): Builder
    }
}