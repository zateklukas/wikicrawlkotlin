package com.zatek.wikicrawl.components

import com.zatek.wikicrawl.base.BaseViewModel
import com.zatek.wikicrawl.view_models.*
import dagger.Component
import javax.inject.Scope

@Scope
annotation class ViewModelScope

@ViewModelScope
@Component(dependencies = [(AppComponent::class)], modules = [])
interface ViewModelInjector {

    fun inject(viewModel: ArticleListViewModel)
    fun inject(viewModel: ArticleListItemViewModel)
    fun inject(viewModel: ArticleViewModel)
    fun inject(viewModel: ArticleImageViewModel)
    fun inject(viewModel: SavedArticlesListViewModel)

}