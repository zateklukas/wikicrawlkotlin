package com.zatek.wikicrawl.api

import com.zatek.wikicrawl.model.ArticleResponse
import com.zatek.wikicrawl.model.ArticleImage
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticleApi {
    @GET("/w/api.php?format=json&action=query&generator=search&gsrsort=relevance&gsrnamespace=0&gsrlimit=10&prop=pageimages|extracts&pilimit=max&exintro&explaintext&exsentences=1&exlimit=max&pilicense=any")
    fun getArticles(@Query("gsrsearch") title: String, @Query("gsroffset") offset: Int): Observable<ArrayList<ArticleResponse>>
    @GET("/w/api.php?format=json&action=query&prop=extracts|images&exintro&explaintext&redirects=1&pilicense=any")
    fun getArticleDetail(@Query("pageids") id: Int) : Observable<ArrayList<ArticleResponse>>
    @GET("/w/api.php?action=query&prop=imageinfo&iiprop=url&format=json")
    fun getImage(@Query("titles")imageTitle: String): Observable<ArticleImage>
}