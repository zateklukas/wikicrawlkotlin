package com.zatek.wikicrawl

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.zatek.wikicrawl.base.BaseActivity
import com.zatek.wikicrawl.base.BaseViewModelFactory
import com.zatek.wikicrawl.databinding.ActivityMainBinding
import com.zatek.wikicrawl.fragments.SavedFragment
import com.zatek.wikicrawl.fragments.SearchFragment
import com.zatek.wikicrawl.view_models.MainViewModel


class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    private lateinit var searchFragment: SearchFragment
    private lateinit var savedFragment: SavedFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val context = this

        viewModel = ViewModelProviders.of(this, BaseViewModelFactory{ MainViewModel(context) }).get(MainViewModel::class.java)

        searchFragment = SearchFragment()
        savedFragment = SavedFragment()

        bind()
        setObservers()
        setListeners()
    }

    override fun bind(){
        binding.viewModel = viewModel
    }

    override fun setObservers() {
        binding.viewModel!!.selectedPage.observe(this, Observer {
            when (it) {
                0 -> {
                    binding.savedMenuButton.alpha = .3f
                    binding.searchMenuButton.alpha = 1f
                    transition(searchFragment)
                }
                1-> {
                    binding.savedMenuButton.alpha = 1f
                    binding.searchMenuButton.alpha = .3f
                    transition(savedFragment)
                }
            }
        })
    }

    override fun setListeners() {
        binding.searchMenuButton.setOnClickListener {
            binding.viewModel!!.selectedPage.value = 0
        }
        binding.savedMenuButton.setOnClickListener {
            binding.viewModel!!.selectedPage.value = 1
        }
    }

    private fun transition(fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
