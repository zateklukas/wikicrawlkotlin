package com.zatek.wikicrawl.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.zatek.wikicrawl.R
import com.zatek.wikicrawl.databinding.ArticleImageBinding
import com.zatek.wikicrawl.model.ArticleImage
import com.zatek.wikicrawl.view_models.ArticleImageViewModel

class ArticleImageAdapter(private val context: Context) : RecyclerView.Adapter<ArticleImageAdapter.ViewHolder>() {

    private lateinit var articleImages: List<String>

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(articleImages[position])
    }

    override fun getItemCount(): Int {
        return if (::articleImages.isInitialized) articleImages.size else 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ArticleImageBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.article_image, parent, false)
        return ViewHolder(binding, context)
    }


    fun updateImages(it: ArrayList<ArticleImage>?) {
        it?.let { images ->
            this.articleImages = images.map { it.url }
            notifyDataSetChanged()
        }
    }


    class ViewHolder(private val binding: ArticleImageBinding, context: Context) :
        RecyclerView.ViewHolder(binding.root) {
        private val viewModel = ArticleImageViewModel(context)

        fun bind(image: String) {
            viewModel.bind(image)
            binding.viewModel = viewModel
        }
    }
}