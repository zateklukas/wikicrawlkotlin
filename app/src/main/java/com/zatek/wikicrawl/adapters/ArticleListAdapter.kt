package com.zatek.wikicrawl.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.zatek.wikicrawl.ArticleActivity
import com.zatek.wikicrawl.R
import com.zatek.wikicrawl.databinding.ItemArticleBinding
import com.zatek.wikicrawl.model.ArticleResponse
import com.zatek.wikicrawl.view_models.ArticleListItemViewModel

class ArticleListAdapter(private var context: Context) : RecyclerView.Adapter<ArticleListAdapter.ViewHolder>() {
    private lateinit var articleList: ArrayList<ArticleResponse>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemArticleBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_article, parent, false)

        return ViewHolder(binding, context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(articleList[position])
    }

    override fun getItemCount(): Int {
        return if (::articleList.isInitialized) articleList.size else 0
    }

    fun updateList(articleList: ArrayList<ArticleResponse>) {
        this.articleList = articleList
        notifyDataSetChanged()
    }

    fun appendList(articleList: ArrayList<ArticleResponse>, offset: Int) {
        this.articleList.addAll(articleList)
        notifyItemRangeInserted(offset, offset + 10)
    }

    class ViewHolder(private val binding: ItemArticleBinding, private var context: Context) :
        RecyclerView.ViewHolder(binding.root) {
        private val viewModel = ArticleListItemViewModel(context)

        fun bind(article: ArticleResponse) {
            viewModel.bind(article)
            binding.viewModel = viewModel
            binding.articleItemBody.setOnClickListener { handleOnClick(article) }
        }

        private fun handleOnClick(article: ArticleResponse): Boolean {
            var intent = Intent(context, ArticleActivity::class.java)
            intent.putExtra("article_id", article.id)
            context.startActivity(intent)
            return true
        }
    }
}